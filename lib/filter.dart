import 'package:flutter/material.dart';

class Filter extends StatefulWidget {

  const Filter({Key? key}) : super(key: key);

  @override
  _FilterState createState() => _FilterState();
}

class _FilterState extends State<Filter> {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.grey,
      padding: EdgeInsets.only(top: size.height * 0.3, bottom: size.height * 0.3, left: size.width * 0.1, right: size.width * 0.1),
      child: Card(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("${selectedDate.toLocal()}".split(' ')[0]),
                  SizedBox(height: 5.0,),
                  IconButton(onPressed:() => _selectDate(context),
                      icon: Icon(Icons.calendar_today)),
                ],
              ),
              RaisedButton(
                onPressed: () => _selectDate(context),
                child: Text('Select date'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
