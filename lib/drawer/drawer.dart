// @dart=2.9
import 'package:flutter/material.dart';
import 'package:ui_just_billing/drawer/data.dart';

class DrawerList extends StatefulWidget {

  @override
  _DrawerListState createState() => _DrawerListState();
}

class _DrawerListState extends State<DrawerList> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(top: size.height * .06, left: size.width * .03, right: size.width * .02),
            height: size.height * .30,
            decoration: BoxDecoration(
              color: Color(0XFFEE8989),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30),
              ),
            ),
            child: Container(
              child: Flex(
                crossAxisAlignment: CrossAxisAlignment.start,
                direction: Axis.horizontal,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 15),
                                blurRadius: 30,
                                color: Color(0xFF666666).withOpacity(.15),
                              ),
                            ],
                          ),
                          child: CircleAvatar(
                            radius: 30.0,
                            backgroundColor: Color(0XFFC4C4C4),
                          ),
                        ),
                        SizedBox(height: size.height*0.02,),
                        Text("Company name", style: TextStyle(fontSize: 18),),
                        SizedBox(height: size.height*0.02,),
                        Text("99994567823", style: TextStyle(fontSize: 18),),
                        SizedBox(height: size.height*0.02,),
                        Text("Email id", style: TextStyle(fontSize: 18),),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          DrawerOption("TRANSACTIONS", Transaction()),
          Divider(color: Colors.black,),
          DrawerOption("MASTERS", Masters()),
          Divider(color: Colors.black,),
          DrawerOption("REPORTS", Reports()),
          Divider(color: Colors.black,),
          DrawerOption("SETTINGS", Settings()),
        ],
      ),
    );
  }
}

class DrawerOption extends StatelessWidget {

  final String text;
  final Widget abc;

  DrawerOption(this.text, this.abc);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(top: size.height * .02, left: size.width * .05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(text, style: TextStyle(fontSize: 20),),
          Container(
            child: Padding(
              padding: EdgeInsets.only(top: size.height * .025, left: size.width * .035, right: size.width * .02),
              child: abc,
            ),
          ),
        ],),
    );
  }
}
