import 'package:flutter/material.dart';
import 'package:ui_just_billing/Masters/product.dart';
import 'package:ui_just_billing/Masters/product brands.dart';
import 'package:ui_just_billing/Masters/product categories.dart';
import 'package:ui_just_billing/Masters/customers.dart';
import 'package:ui_just_billing/Masters/consumer.dart';
import 'package:ui_just_billing/Masters/discount.dart';
import 'package:ui_just_billing/Masters/taxgroup.dart';
import 'package:ui_just_billing/Masters/taxrates.dart';
import 'package:ui_just_billing/Masters/departments.dart';
import 'package:ui_just_billing/Masters/units.dart';
import 'package:ui_just_billing/Masters/suppliers.dart';
import 'package:ui_just_billing/Masters/agents.dart';
import 'package:ui_just_billing/Masters/Booking.dart';
import 'package:ui_just_billing/Masters/pricecatalog.dart';

class Transaction extends StatefulWidget {
  const Transaction({Key? key}) : super(key: key);

  @override
  _TransactionState createState() => _TransactionState();
}

class _TransactionState extends State<Transaction> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //padding: EdgeInsets.only(top: size.height * .02, left: size.width * .05),
        Text("Invoices", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Orders", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Purchase", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Estimations", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Returns", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Pending Dispatches", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Inverntory Adjustments", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Production", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Expenses", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Customer Feedback", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
      ],
    );
  }
}

class Masters extends StatefulWidget {
  const Masters({Key? key}) : super(key: key);

  @override
  _MastersState createState() => _MastersState();
}

class _MastersState extends State<Masters> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //padding: EdgeInsets.only(top: size.height * .02, left: size.width * .05),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Products()));
            },
            child: Text("Product", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ProductCategories()));
            },
            child: Text("Product Categories", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ProductBrands()));
            },
            child: Text("Product Brands", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Customer()));
            },
            child: Text("Customer Business", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Consumer()));
            },
            child: Text("Customer Consumer", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Discount()));
            },
            child: Text("Discount", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => TaxGroup()));
            },
            child: Text("Tax Groups", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => TaxRates()));
            },
            child: Text("Tax Rates", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Departments()));
            },
            child: Text("Departments", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Units()));
            },
            child: Text("Units", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Suppliers()));
            },
            child: Text("Suppliers", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Agents()));
            },
            child: Text("Agents", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Bookings()));
            },
            child: Text("Bookings", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
        GestureDetector(
            onTap: (){
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => PriceCatalog()));
            },
            child: Text("Price Catalog", style: TextStyle(fontSize: 18),)),
        SizedBox(height: size.height*0.020,),
      ],
    );
  }
}

class Reports extends StatefulWidget {
  const Reports({Key? key}) : super(key: key);

  @override
  _ReportsState createState() => _ReportsState();
}

class _ReportsState extends State<Reports> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Detailed Sales Invoice", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Product Stock", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Product Summary", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Hourly Sales", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("End of Day", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("GST Dashboard", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Tax Summary", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Cancel Order Analysis Report", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Sales Summary By Sales Person", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Detailed Sales Order", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Product Sold By Sales Person", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Detailed Returned Order", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Complementary Sales", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Customer Ledger", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Detailed Purchase Invoice", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
      ],
    );
  }
}

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //padding: EdgeInsets.only(top: size.height * .02, left: size.width * .05),
        Text("App Settings", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Business Details", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("Diagnostics", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
        Text("User Management", style: TextStyle(fontSize: 18),),
        SizedBox(height: size.height*0.020,),
      ],
    );
  }
}


