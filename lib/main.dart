// @dart=2.9
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ui_just_billing/widgets/rounded_button.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:ui_just_billing/drawer/drawer.dart';
import 'package:ui_just_billing/filter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}


class MyHomePage extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Color(0xFFD85858),
        title: Text("Company Name"),
        actions: <Widget>[

          IconButton(
            icon: Icon(
              Icons.filter_alt,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Filter()));
            },
          ),
          IconButton(
            icon: Icon(
              Icons.refresh,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          ),
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      drawer: Drawer(
        child: DrawerList(),
    ),
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          width: double.infinity,
          child: SingleChildScrollView(
            child: Container(
                  height: size.height,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: size.height * 0.05,),
                       Wrap(
                         children: [Row(
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: [
                             Circle("Invoice"),
                             Circle("Order"),
                             Circle("Purchase"),
                           ],
                         ),
  ],
                       ),
                      //SizedBox(height: size.height * 0.02,),
                      Divider(color: Colors.black,),
                      SizedBox(height: size.height * 0.02,),
                      Wrap(
                        children: [Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Circle("Products"),
                            Circle("Customers"),
                            Circle("Suppliers"),
                          ],
                        ),
                        ],
                      ),
                      Divider(color: Colors.black,),
                      SizedBox(height: size.height * 0.01,),
                      IntrinsicHeight (
                        child:
                          Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            VerticalCard("Sales\n", "Rs 0\n", "0 Invoices"),
                            VerticalDivider(color: Colors.black,),
                            VerticalCard("Expenses\n", "Rs 0\n", "0 Vouchers"),
                            VerticalDivider(color: Colors.black,),
                            VerticalCard("Products\n", "2\n", "Out of Stock"),
                          ],
                        ),
                      ),
                      SizedBox(height: size.height * 0.01,),
                      Divider(color: Colors.black,),
                      SizedBox(height: size.height * 0.02,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: size.width*0.07),
                            child: Container(
                                child: Text("Sales Trend", style: TextStyle(fontSize: 18), textAlign: TextAlign.left,)),
                          ),
                          Center(
                            child: Container(
                              height: size.height * .3,
                                child: SfCartesianChart(
                                    primaryXAxis: CategoryAxis(),
                                    series: <LineSeries<SalesData, String>>[
                                      LineSeries<SalesData, String>(),
                                    ]
                                ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: size.height * 0.1,)
                    ],
                  ),
                ),
              ),
          ),
        ),
      ),
    );// This trailing comma makes auto-formatting nicer for build methods.
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}

class Circle extends StatelessWidget {
  final String text;
  Circle(this.text);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 15),
                blurRadius: 30,
                color: Color(0xFF666666).withOpacity(.15),
              ),
            ],
          ),
          child: CircleAvatar(
            radius: 50.0,
            backgroundColor: Color(0XFFC4C4C4),
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width * .25,
          child:
          RoundedButton(
            text: text,
            fontSize: 13,
            press: () {
              // Navigator.push(
              //     context, MaterialPageRoute(builder: (context) => Authenticate()));
            },
          ),
        ),
      ],
    );
  }
}

class VerticalCard extends StatelessWidget {

  final String t1,t2,t3;
  VerticalCard(this.t1, this.t2, this.t3);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .20,
      width: size.width * .25,
      child: Card(
        elevation: 8,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        color: Color(0XFFF8CDCD),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Container(
            height: size.height * .45,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  t1,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                  textAlign: TextAlign.center,
                ),
                Text(
                  t2,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                  textAlign: TextAlign.center,
                ),
                Text(
                  t3,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

showDialogFunc(context, img, name, state)
{
  return showDialog(
      context: context,
      builder: (context){
        return Center(
          child: Material(
            type: MaterialType.transparency,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              padding: EdgeInsets.all(15),
              width: MediaQuery.of(context).size.width * 0.7,
              height: 320,
              child: Wrap(
                children: [Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Image.network(img, width: 200, height: 200),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Text(
                      name,
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10,),
                    Text(
                      state,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                      ),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                ],
              ),
            ),
          ),
        );
      }
  );
}

