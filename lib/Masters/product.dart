import 'package:flutter/material.dart';
import 'package:ui_just_billing/Masters/addproducts.dart';
import 'package:ui_just_billing/Masters/editproducts.dart';
import 'dart:math';

class Products extends StatelessWidget {
  const Products({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Products"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          ),
          Transform.rotate(
            angle: 90 * pi/180,
            child: IconButton(
              icon: Icon(
                Icons.compare_arrows,
                color: Colors.white,
              ),
              onPressed: null,
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => EditProduct()));
    },
    child: const Icon(Icons.add),
    backgroundColor: Color(0xFFD85858)
        ,
    ),
    );
  }
}
