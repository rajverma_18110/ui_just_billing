import 'package:flutter/material.dart';

class AddProductBrand extends StatefulWidget {
  const AddProductBrand({Key? key}) : super(key: key);

  @override
  _AddProductBrandState createState() => _AddProductBrandState();
}

class _AddProductBrandState extends State<AddProductBrand> {
  String dropdownValue = 'one';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Product Brands"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("SAVE"),
                SizedBox(width: 20,),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
              children : [
                SizedBox(height: size.height * 0.05,),
                Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 15),
                        blurRadius: 30,
                        color: Color(0xFF666666).withOpacity(.15),
                      ),
                    ],
                  ),
                  child: CircleAvatar(
                    radius: 50.0,
                    backgroundColor: Color(0XFFC4C4C4),
                  ),
                ),
                SizedBox(height: size.height * 0.05,),
                TextFormField(
                  validator: (value) {
                    if(value.toString().isEmpty)
                      return 'Enter Product name';
                    return null;
                  },
                  cursorColor: Colors.black,
                  style: TextStyle(color: Colors.black, fontSize: 16,),
                  decoration: InputDecoration(
                    labelText: 'Brand Name',
                    labelStyle: TextStyle(
                      color: Colors.black,
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                  ),
                ),
                TextFormField(
                  validator: (value) {
                    if(value.toString().isEmpty)
                      return 'Enter Product name';
                    return null;
                  },
                  cursorColor: Colors.black,
                  style: TextStyle(color: Colors.black, fontSize: 16,),
                  decoration: InputDecoration(
                    labelText: 'Manufacturer',
                    labelStyle: TextStyle(
                      color: Colors.black,
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Text("Discount Name", style: TextStyle(fontSize: 16),)),
                    Container(
                      width: size.width * 0.5,
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: null,
                        icon: const Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        elevation: 16,
                        style: const TextStyle(color: Colors.black),
                        underline: Container(
                          height: 1,
                          width: 40,
                          color: Colors.grey,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            dropdownValue = newValue!;
                          });
                        },
                        items: <String>['One', 'Two', 'Free', 'Four']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    // IconButton(
                    //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Description", style: TextStyle(fontSize: 16),),
                    SizedBox(width: 5,),
                  ],
                ),
                SizedBox(height: 50,),
                Divider(color: Colors.black),
              ]),
        ),
      ),
    );
  }
}
