import 'package:flutter/material.dart';

class AddBooking extends StatefulWidget {
  const AddBooking({Key? key}) : super(key: key);

  @override
  _AddBookingState createState() => _AddBookingState();
}

class _AddBookingState extends State<AddBooking> {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  String dropdownValue = 'one';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Bookings"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("SAVE"),
                SizedBox(width: 20,),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children : [
              SizedBox(height: size.height * 0.02,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: size.width * 0.77,
                    child: TextFormField(
                      validator: (value) {
                        if(value.toString().isEmpty)
                          return 'Enter Product name';
                        return null;
                      },
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black, fontSize: 16,),
                      decoration: InputDecoration(
                        labelText: 'Booking Date',
                        labelStyle: TextStyle(
                          color: Colors.black,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0,),
                  IconButton(onPressed:() => _selectDate(context),
                      icon: Icon(Icons.calendar_today)),
                ],
              ),
              SizedBox(height: 10,),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Enter Product name';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Name',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Enter Product name';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Phone Number',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'No of Guests',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("Status", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.5,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: null,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 1,
                        width: 40,
                        color: Colors.grey,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>['One', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Remarks',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(height: 50,),
            ],
          ),
        ),
      ),
    );
  }
}
