import 'package:flutter/material.dart';
import 'package:ui_just_billing/widgets/rounded_button.dart';

class EditProduct extends StatefulWidget {
  const EditProduct({Key? key}) : super(key: key);

  @override
  _EditProductState createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  String dropdownValue = 'one';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Products"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("EDIT"),
                SizedBox(width: 20,),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
              children : [
                SizedBox(height: size.height * 0.05,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 15),
                              blurRadius: 30,
                              color: Color(0xFF666666).withOpacity(.15),
                            ),
                          ],
                        ),
                        child: CircleAvatar(
                          radius: 75.0,
                          backgroundColor: Color(0XFFC4C4C4),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Code", style: TextStyle(fontSize: 18, color: Colors.grey),),
                          SizedBox(height: 5,),
                          Text("hdeoif", style: TextStyle(fontSize: 18),),
                          SizedBox(height: 20,),
                          Text("Category", style: TextStyle(fontSize: 18, color: Colors.grey),),
                          SizedBox(height: 5,),
                          Text("hdeoif", style: TextStyle(fontSize: 18),),
                          SizedBox(height: 20,),
                          Text("Brand", style: TextStyle(fontSize: 18, color: Colors.grey),),
                          SizedBox(height: 5,),
                          Text("hdeoif", style: TextStyle(fontSize: 18),),
                        ],
                      ),
                      SizedBox(width: 5,),
                    ],
                  ),
                ),
                SizedBox(height: 30,),
                Text("Product Description", style: TextStyle(fontSize: 18, color: Colors.grey),),
                SizedBox(height: 5,),
                Text("hdeoif", style: TextStyle(fontSize: 18),),
                SizedBox(height: 20,),
                Text("Unit", style: TextStyle(fontSize: 18, color: Colors.grey),),
                SizedBox(height: 5,),
                Text("Box", style: TextStyle(fontSize: 18),),
                SizedBox(height: 20,),
                Text("Allow Negative Stock", style: TextStyle(fontSize: 18, color: Colors.grey),),
                SizedBox(height: 5,),
                Text("Yes", style: TextStyle(fontSize: 18),),
                SizedBox(height: 16,),
                Elements(),
                Elements(),
                Elements(),
                Elements(),
              ]),
        ),
      ),
    );
  }
}

class Elements extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .10,
      width: size.width * .95,
      child: Card(
        color: Color(0xFFFCE0E0),
        elevation: 5.0,
        child: Container(
          //width: size.width * .60,
          child: ListTile(
            onTap: (){},
            title: Container(
              padding: EdgeInsets.all(10),
              child: Container(
                width: size.width * .55,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Standard Purchase Price", style: TextStyle(fontSize: 14),),
                        SizedBox(height: 8,),
                        Text("Rs. 0.00", style: TextStyle(fontSize: 14),),
                      ],
                    ),
                OutlinedButton(
                  onPressed: () {
                    print('Received click');
                  },
                  child: const Text('Non Fixed Price', style: TextStyle(color: Colors.black, fontSize: 12),),
                  style: OutlinedButton.styleFrom(
                    side: BorderSide(color: Colors.black),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                  ),
                ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
