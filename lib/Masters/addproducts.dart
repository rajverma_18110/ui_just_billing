import 'package:flutter/material.dart';

class AddProducts extends StatefulWidget {
  const AddProducts({Key? key}) : super(key: key);

  @override
  _AddProductsState createState() => _AddProductsState();
}

class _AddProductsState extends State<AddProducts> {
  String dropdownValue = 'one';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Products"),
        actions: <Widget>[
         Padding(
           padding: const EdgeInsets.symmetric(horizontal: 8.0),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
             crossAxisAlignment: CrossAxisAlignment.center,
             children: [
               Text("SAVE"),
               SizedBox(width: 10,),
               Text("PRICING"),
             ],
           ),
         ),
        ],
      ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
      children : [
          SizedBox(height: size.height * 0.05,),
            Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, 15),
                  blurRadius: 30,
                  color: Color(0xFF666666).withOpacity(.15),
                ),
              ],
            ),
            child: CircleAvatar(
              radius: 50.0,
              backgroundColor: Color(0XFFC4C4C4),
            ),
      ),
            SizedBox(height: size.height * 0.05,),
            TextFormField(
              validator: (value) {
                if(value.toString().isEmpty)
                  return 'Enter Product name';
                return null;
              },
              cursorColor: Colors.black,
              style: TextStyle(color: Colors.black, fontSize: 16,),
              decoration: InputDecoration(
                labelText: 'Product Name',
                labelStyle: TextStyle(
                  color: Colors.black,
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
              ),
            ),
          TextFormField(
            validator: (value) {
              if(value.toString().isEmpty)
                return 'Enter Product name';
              return null;
            },
            cursorColor: Colors.black,
            style: TextStyle(color: Colors.black, fontSize: 16,),
            decoration: InputDecoration(
              labelText: 'Product Code',
              labelStyle: TextStyle(
                color: Colors.black,
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
            ),
          ),
          TextFormField(
            validator: (value) {
              if(value.toString().isEmpty)
                return 'Enter Product name';
              return null;
            },
            cursorColor: Colors.black,
            style: TextStyle(color: Colors.black, fontSize: 16,),
            decoration: InputDecoration(
              labelText: 'HCN Code',
              labelStyle: TextStyle(
                color: Colors.black,
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
            ),
          ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("Parent Product", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.5,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: null,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 1,
                        width: 40,
                        color: Colors.grey,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>['One', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 10),
                child: Text("Category", style: TextStyle(fontSize: 16),)),
            Container(
              width: size.width * 0.5,
              child: DropdownButton<String>(
                isExpanded: true,
                value: null,
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.black),
                underline: Container(
                  height: 1,
                  width: 40,
                  color: Colors.grey,
                ),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['One', 'Two', 'Free', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            // IconButton(
            //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
          ],
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 10),
                child: Text("Discount", style: TextStyle(fontSize: 16),)),
            Container(
              width: size.width * 0.5,
              child: DropdownButton<String>(
                isExpanded: true,
                value: null,
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.black),
                underline: Container(
                  height: 1,
                  width: 40,
                  color: Colors.grey,
                ),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['One', 'Two', 'Free', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            // IconButton(
            //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
          ],
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 10),
                child: Text("Department", style: TextStyle(fontSize: 16),)),
            Container(
              width: size.width * 0.5,
              child: DropdownButton<String>(
                isExpanded: true,
                value: null,
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.black),
                underline: Container(
                  height: 1,
                  width: 40,
                  color: Colors.grey,
                ),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['One', 'Two', 'Free', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            // IconButton(
            //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
          ],
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 10),
                child: Text("Unit", style: TextStyle(fontSize: 16),)),
            Container(
              width: size.width * 0.5,
              child: DropdownButton<String>(
                isExpanded: true,
                value: null,
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.black),
                underline: Container(
                  height: 1,
                  width: 40,
                  color: Colors.grey,
                ),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['One', 'Two', 'Free', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            // IconButton(
            //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
          ],
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 10),
                child: Text("Brand", style: TextStyle(fontSize: 16),)),
            Container(
              width: size.width * 0.5,
              child: DropdownButton<String>(
                isExpanded: true,
                value: null,
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.black),
                underline: Container(
                  height: 1,
                  width: 40,
                  color: Colors.grey,
                ),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['One', 'Two', 'Free', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            // IconButton(
            //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 10),
                child: Text("Product Type", style: TextStyle(fontSize: 16),)),
            Container(
              width: size.width * 0.5,
              child: DropdownButton<String>(
                isExpanded: true,
                value: null,
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.black),
                underline: Container(
                  height: 1,
                  width: 40,
                  color: Colors.grey,
                ),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['One', 'Two', 'Free', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            // IconButton(
            //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
          ],
        ),
        SideRow("Allow Negative Stock"),
        SideRow("Return Allowed"),
        SideRow("Active"),
        SideRow("Purchase Item"),
        SideRow("Sales Item"),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Product Description", style: TextStyle(fontSize: 16),),
            SizedBox(width: 5,),
          ],
        ),
        SizedBox(height: 30,),
        Divider(color: Colors.grey,),
        SizedBox(height: 20,),
    ],
    ),
          ),
        ),
    );
  }
}

class SideRow extends StatelessWidget {
  String text;
  SideRow(this.text);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Row(
      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: size.width * 0.45,
            padding: EdgeInsets.only(top: 10),
            child: Text(text, style: TextStyle(fontSize: 16),)),
        IconButton(
            onPressed:(){}, icon: Icon(Icons.check_box, color: Color(0xFF777BF0),)),
        //SizedBox(width: size.width * 0.1,),
      ],
    );
  }
}

