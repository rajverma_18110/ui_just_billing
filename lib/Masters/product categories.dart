import 'package:flutter/material.dart';
import 'package:ui_just_billing/Masters/addproductcategory.dart';
import 'dart:math';

class ProductCategories extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Product Category"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddProductCategory()));
        },
        child: const Icon(Icons.add),
        backgroundColor: Color(0xFFD85858),
      ),
    );
  }
}
