import 'package:flutter/material.dart';

class AddTaxRates extends StatefulWidget {
  const AddTaxRates({Key? key}) : super(key: key);

  @override
  _AddTaxRatesState createState() => _AddTaxRatesState();
}

class _AddTaxRatesState extends State<AddTaxRates> {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  String dropdownValue = 'VOLUME';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Tax"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("SAVE"),
                SizedBox(width: 20,),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children : [
              SizedBox(height: size.height * 0.02,),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Enter Product name';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Tax Name',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("Tax Type", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.5,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: null,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 1,
                        width: 40,
                        color: Colors.grey,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>['One', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("Input Ledger", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.5,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: null,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 1,
                        width: 40,
                        color: Colors.grey,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>['One', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("Tax Rate", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.4,
                    child: TextFormField(
                      validator: (value) {
                        if(value.toString().isEmpty)
                          return 'Enter Product name';
                        return null;
                      },
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black, fontSize: 16,),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(
                          color: Colors.black,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),
          Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  width: size.width * 0.45,
                  padding: EdgeInsets.only(top: 10),
                  child: Text("Active", style: TextStyle(fontSize: 16),)),
              IconButton(
                  onPressed:(){}, icon: Icon(Icons.check_box, color: Colors.black,)),
            ],
          ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      width: size.width * 0.45,
                      padding: EdgeInsets.only(top: 10),
                      child: Text("Returnable", style: TextStyle(fontSize: 16),)),
                  IconButton(
                      onPressed:(){}, icon: Icon(Icons.check_box_outline_blank, color: Colors.black,)),
                ],
              ),

              SizedBox(height: 80,),
            ],
          ),
        ),
      ),
    );
  }
}
