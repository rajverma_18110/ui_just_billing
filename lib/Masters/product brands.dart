// @dart=2.9

import 'package:flutter/material.dart';
import 'package:ui_just_billing/Masters/addproductbrands.dart';
import 'dart:math';

class ProductBrands extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Product Brands"),
      ),
      body: Container(
                padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Elements(),
                    Elements(),
                    Elements(),
                    Elements(),
                  ],
                ),
              ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddProductBrand()));
        },
        child: const Icon(Icons.add),
        backgroundColor: Color(0xFFD85858)
        ,
      ),
    );
  }
}

class Elements extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .16,
      width: size.width * .95,
      child: Card(
        color: Color(0xFFFCE0E0),
        elevation: 5.0,
        child: Container(
          //width: size.width * .60,
          child: ListTile(
            onTap: (){},
            title: Container(
              padding: EdgeInsets.all(10),
              child: Container(
                width: size.width * .55,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Brand Name"),
                    SizedBox(height: 4,),
                    Text("Manufacturer"),
                    SizedBox(height: 4,),
                    Text("Discount"),
                    SizedBox(height: 4,),
                    Text("0 Product"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

