import 'package:flutter/material.dart';

class EditCustomer extends StatefulWidget {
  const EditCustomer({Key? key}) : super(key: key);

  @override
  _EditCustomerState createState() => _EditCustomerState();
}

class _EditCustomerState extends State<EditCustomer> {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  String dropdownValue = 'one';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Customer"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("EDIT"),
                SizedBox(width: 20,),
              ],
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.menu,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children : [
              SizedBox(height: size.height * 0.04,),
              Text("Hdeoif", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
              SizedBox(height: 10,),
              Text("Phone Number", style: TextStyle(fontSize: 18,),),
              SizedBox(height: 10,),
              Text("Customer Since Date", style: TextStyle(fontSize: 18,),),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Date of Birth : Date", style: TextStyle(fontSize: 16),),
                ],
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Anniversary : Date", style: TextStyle(fontSize: 16),),
                ],
              ),
              SizedBox(height: 20,),
              Divider(color: Colors.black,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: size.height * 0.08,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Order values"),
                        SizedBox(height: 10,),
                        Text("Rs 0.00"),
                      ],
                    ),
                  ),
                  Container(
                      height: size.height * 0.08,
                      child: VerticalDivider(color: Colors.black,)),
                  Container(
                    height: size.height * 0.08,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Order values"),
                        SizedBox(height: 10,),
                        Text("Rs 0.00"),
                      ],
                    ),
                  ),
                  Container(
                      height: size.height * 0.08,
                      child: VerticalDivider(color: Colors.black,)),
                  Container(
                    height: size.height * 0.08,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Order values"),
                        SizedBox(height: 10,),
                        Text("Rs 0.00"),
                      ],
                    ),
                  ),
                ],
              ),
              Divider(color: Colors.black,),
              SizedBox(height: 10,),
              Text("Address", style: TextStyle(fontSize: 16,),),
              SizedBox(height: 80,),
            ],
          ),
        ),
      ),
    );
  }
}
