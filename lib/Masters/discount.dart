// @dart=2.9

import 'package:flutter/material.dart';
import 'package:ui_just_billing/Masters/adddiscount.dart';
import 'dart:math';

class Discount extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Discount"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          ),
          Transform.rotate(
            angle: 90 * pi/180,
            child: IconButton(
              icon: Icon(
                Icons.compare_arrows,
                color: Colors.white,
              ),
              onPressed: null,
            ),
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(top: 15, left: 10, right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Elements(),
            Elements(),
            Elements(),
            Elements(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddDiscount()));
        },
        child: const Icon(Icons.add),
        backgroundColor: Color(0xFFD85858)
        ,
      ),
    );
  }
}

class Elements extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .13,
      width: size.width * .95,
      child: Card(
        color: Color(0xFFFCE0E0),
        elevation: 5.0,
        child: Container(
          //width: size.width * .60,
          child: ListTile(
            onTap: (){},
            title: Container(
              padding: EdgeInsets.all(10),
              child: Container(
                width: size.width * .55,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Discount Title"),
                    SizedBox(height: 4,),
                    Text("Percentage"),
                    SizedBox(height: 4,),
                    Text("Volume"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

