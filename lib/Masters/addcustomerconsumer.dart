import 'package:flutter/material.dart';

class AddCustomerConsumer extends StatefulWidget {
  const AddCustomerConsumer({Key? key}) : super(key: key);

  @override
  _AddCustomerConsumerState createState() => _AddCustomerConsumerState();
}

class _AddCustomerConsumerState extends State<AddCustomerConsumer> {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  String dropdownValue = 'one';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFD85858),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Customers"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("SAVE"),
                SizedBox(width: 20,),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children : [
              SizedBox(height: size.height * 0.02,),
              Container(
                child: Text("ACCOUNT INFORMATION", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: size.width * 0.4,
                    child: TextFormField(
                      validator: (value) {
                        if(value.toString().isEmpty)
                          return 'Enter Product name';
                        return null;
                      },
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black, fontSize: 16,),
                      decoration: InputDecoration(
                        labelText: 'First Name',
                        labelStyle: TextStyle(
                          color: Colors.black,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: size.width *0.4,
                    child: TextFormField(
                      validator: (value) {
                        if(value.toString().isEmpty)
                          return 'Enter Product name';
                        return null;
                      },
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black, fontSize: 16,),
                      decoration: InputDecoration(
                        labelText: 'Last Name',
                        labelStyle: TextStyle(
                          color: Colors.black,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: size.width * 0.77,
                    child: TextFormField(
                      validator: (value) {
                        if(value.toString().isEmpty)
                          return 'Enter Product name';
                        return null;
                      },
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black, fontSize: 16,),
                      decoration: InputDecoration(
                        labelText: 'Date of Birth',
                        labelStyle: TextStyle(
                          color: Colors.black,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0,),
                  IconButton(onPressed:() => _selectDate(context),
                      icon: Icon(Icons.calendar_today)),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: size.width * 0.77,
                    child: TextFormField(
                      validator: (value) {
                        if(value.toString().isEmpty)
                          return 'Enter Product name';
                        return null;
                      },
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black, fontSize: 16,),
                      decoration: InputDecoration(
                        labelText: 'Anniversary',
                        labelStyle: TextStyle(
                          color: Colors.black,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0,),
                  IconButton(onPressed:() => _selectDate(context),
                      icon: Icon(Icons.calendar_today)),
                ],
              ),
              SizedBox(height: 10,),
              Container(
                child: Text("CONTACT INFORMATION", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Enter Product name';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Mobile Number',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Enter Product name';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Alternate Phone Number',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                child: Text("ADDRESS INFORMATION", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("Country", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.5,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: null,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 1,
                        width: 40,
                        color: Colors.grey,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>['One', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("State", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.5,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: null,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 1,
                        width: 40,
                        color: Colors.grey,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>['One', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Text("City", style: TextStyle(fontSize: 16),)),
                  Container(
                    width: size.width * 0.5,
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: null,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 1,
                        width: 40,
                        color: Colors.grey,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>['One', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  // IconButton(
                  //     onPressed:(){}, icon: Icon(Icons.arrow_drop_down)),
                ],
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Area',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Pincode',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Street Name',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Door No.',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Address',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              SizedBox(height: 10,),
              Container(
                child: Text("REGISTRATION INFORMATION", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'PAN',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Aadhar',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Referrrer Mobile',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Referrer Mobile',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Customer Source',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Credit Days',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Credit Limit',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              TextFormField(
                validator: (value) {
                  if(value.toString().isEmpty)
                    return 'Email Id';
                  return null;
                },
                cursorColor: Colors.black,
                style: TextStyle(color: Colors.black, fontSize: 16,),
                decoration: InputDecoration(
                  labelText: 'Initial Outstanding',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),

              SizedBox(height: 50,),
            ],
          ),
        ),
      ),
    );
  }
}
